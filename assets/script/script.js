"use strict";

// monement  *******     ******      **** 

const subtitles = document.querySelectorAll(".subtitles");
const container = document.querySelectorAll(".container");

for (let i = 0; i < subtitles.length; i++) {
  let subtitle = subtitles[i];
  subtitle.addEventListener("click", function () {
    container[i].classList.toggle("hidden");
  });
}

// plats        ******            *********

const displayTitle = function (message) {
  document.querySelector(".plat_name").innerHTML = message;
};
const displayTMessage = function (message) {
  document.querySelector(".plat_discription").innerHTML = message;
};
const T=['Frikasé','Briik','Lablebii','Kafteji','Couscous','Tajine','mloukhiya','salade méchouia','chakchouka'];
const M=[
`des petits sandwichs , préparés avec des petits pains frits . ils sont garnit de pomme terre cuite des oeufs mais vous pouvez éventuellement ajouter du  thon des olives vertes ou noires ou même les garnir de poivron tomates de multitudes de garniture s’y prête merveilleusement     … sans oublier la harissa !`,
`Entrée d'origine maghrébine réalisée à partir d'une feuille de brik composée de farine et de semoule de blé , La brik contient un oeuf accompagné de légumes ou de viande.`,
`Parmi les soupes les plus populaires de la cuisine tunisienne figure également le lablabi, fait de pois chiches, huile d'olive, harissa, ail, sel, cumin, jus de citron et croûtons de pain.`,
`Le kafteji est un plat composé principalement de légumes et il est épicé et pimenté. Les légumes sont cuits un à un en friture, puis mélangés ensemble avec le frite et l'ouef ! .`,
`Le couscous est le plat traditionnel par excellence, surtout pour les cérémonies. D'origine berbère, on le retrouve dans l'ensemble du Maghreb et se compose de semoule, de légumes et de viande - généralement de l'agneau.`,
`Le tajine tunisien ne doit pas être confondu avec le tajine marocain : il s’agit d’un gratin à base d’œufs, de viande ou de thon, de pommes de terre et d’épices qui est cuite au four.`,
`En Tunisie, cette poudre de corète dont la cuisson délicate donne à ce plat toute sa saveur, demande plusieurs heures de mijotage à feu doux. D'abord frite dans l'huile, elle est ensuite délayée avec de l'eau bien chaude et demande alors une certaine force pour que les deux composants (huile et eau) se mêlent jusqu'à former un liquide verdâtre qui deviendra marron foncé au fil de la cuisson lente et des temps de repos de la sauce.`,
`La salade méchouia est faite à base de légumes grillés coupés finement comme le poivron, la tomate ou l'aubergine, servis avec des oignons, du piment, de l'ail, de la coriandre et de carvi en poudre. On l'assaisonne ensuite d'huile d'olive, d'harissa et de jus de citron, avant d'ajouter quelques rondelles d'oeufs dur, des câpres et des miettes de thon.`,
`On retrouve la chakchouka dans de nombreux pays du Maghreb ou de Moyen-Orient. En Tunisie, elle est préparée avec des oeufs mollets, des pommes de terre, des tomates, des oignons, de l'ail et des épicés, le tout revenu dans de l'huile d'olive.`
];

const plat_container=document.querySelectorAll('.plat-container');
for(let i=0; i<plat_container.length ;i++){
    plat_container[i].addEventListener("click", function () {
        displayTitle(T[i]);
        displayTMessage(M[i]);
    });
}